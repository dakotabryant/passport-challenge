export const generateChildren = (min, max, count) => {
  if (count > 15) {
    count = 15;
  }
  return Array.from({ length: count }, () => {
    return Math.floor(Math.random() * (max - min + 1)) + parseInt(min, 10);
  });
};
