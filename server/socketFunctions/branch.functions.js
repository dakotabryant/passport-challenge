import Branch from '../models/branch.model';
import { generateChildren } from '../utils';

export const getBranches = async () => {
  const branches = await Branch.find();
  return branches;
};

export const createBranch = async (title, children, min, max) => {
  const branch = await Branch.create({ title, children, min, max });
  return branch;
};

export const updateBranch = async (id, updateObj) => {
  let newChildren;
  let newBranch;
  const { min, max, count, title } = updateObj;
  const branch = await Branch.findOne({ _id: id });
  const branchMin = min !== '' ? min : branch.min;
  const branchMax = max !== '' ? max : branch.max;
  const branchCount = count !== '' ? count : branch.children.length;
  const branchTitle = title !== '' ? title : branch.title;
  if (
    branch.min !== min ||
    branch.max !== max ||
    branch.children.length !== count
  ) {
    newChildren = await generateChildren(branchMin, branchMax, branchCount);
    newBranch = await Branch.findByIdAndUpdate(
      id,
      {
        title: branchTitle,
        min: branchMin,
        max: branchMax,
        children: newChildren,
      },
      { new: true, runValidators: true }
    );
  } else {
    newBranch = await Branch.findByIdAndUpdate(
      id,
      {
        title: branchTitle,
        min: branchMin,
        max: branchMax,
      },
      { new: true, runValidators: true }
    );
  }
  return newBranch;
};

export const deleteBranch = async (id) => {
  const deletedBranch = await Branch.findByIdAndRemove(id);
  return deletedBranch;
};
