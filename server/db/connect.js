import Mongoose from 'mongoose';

Mongoose.Promise = global.Promise;

const connectToDb = async (uri) => {
  try {
    await Mongoose.connect(
      uri,
      { useMongoClient: true }
    );
    console.log('Connected to mongo!!!');
  } catch (err) {
    console.error('Could not connect to MongoDB');
  }
};

export default connectToDb;
