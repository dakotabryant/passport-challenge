import mongoose from 'mongoose';

const branch = mongoose.Schema({
  title: {
    type: String,
    required: 'Please enter a title',
  },
  children: {
    type: [Number],
    required: 'Please create at least one child node',
  },
  min: {
    type: Number,
    required: 'Please submit a minimum',
  },
  max: {
    type: Number,
    required: 'Please submit a maximum',
  },
});

const Branch = mongoose.model('Branch', branch);

export default Branch;
