import path from 'path';

const config = {};

config.logFileDir = path.join(__dirname, '../../log');
config.logFileName = 'app.log';
config.dbHost = process.env.DB_HOST || 'localhost';
config.serverPort = process.env.serverPort || 8080;

export default config;
