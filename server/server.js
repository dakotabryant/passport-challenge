import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import path from 'path';
import httpServer from 'http';
import socketio from 'socket.io';
import connectToDb from './db/connect';
import config from './config/config.dev';
import {
  getBranches,
  createBranch,
  updateBranch,
  deleteBranch,
} from './socketFunctions';
import { generateChildren } from './utils';

const app = express();
const server = httpServer.Server(app);
const io = socketio(server, { origins: '*:*' });

require('dotenv').config({ path: path.join(__dirname, '../.env') });

const uri = process.env.DB_HOST;

connectToDb(uri);
// utility middleware
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

io.on('connect', (socket) => {
  socket.on('get-branches', async () => {
    const branches = await getBranches();
    socket.emit('get-branches', branches);
  });
  socket.on('create-branch', async ({ title, min, max, count }) => {
    const children = await generateChildren(min, max, count);
    const createdBranch = await createBranch(title, children, min, max);
    io.emit('create-branch', createdBranch);
  });
  socket.on('update-branch', async ({ id, updateObj }) => {
    const updatedBranch = await updateBranch(id, updateObj);
    io.emit('update-branch', updatedBranch);
  });
  socket.on('delete-branch', async ({ id }) => {
    const deletedBranch = await deleteBranch(id);
    io.emit('delete-branch', deletedBranch);
  });
});

server.listen(config.serverPort, () => {
  console.log('server started - ', config.serverPort);
});
