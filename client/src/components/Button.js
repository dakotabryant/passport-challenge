import React from 'react';
import styled from 'styled-components';

const ButtonStyled = styled.button`
  background-color: ${({ backgroundColor }) => backgroundColor};
  border: none;
  height: 50px;
  width: ${({ isCircle }) => (isCircle ? 50 : 150)}px;
  border-radius: ${({ isCircle }) => (isCircle ? '100%' : 0)};
  color: white;
  font-size: 25px;
`;

const Button = ({
  children,
  backgroundColor,
  isCircle,
  className,
  onClick,
}) => {
  return (
    <ButtonStyled
      className={className}
      backgroundColor={backgroundColor}
      isCircle={isCircle}
      onClick={onClick}
    >
      {children}
    </ButtonStyled>
  );
};

export default Button;
