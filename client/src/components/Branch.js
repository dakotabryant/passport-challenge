import React, { Component } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import Button from './Button';
import { toggleModal } from '../actions';

const StyledBranch = styled.div`
  display: flex;
  margin-top: 50px;
  hr {
    width: 50px;
    border: none;
    border-top: 2px dotted black;
    margin: 0;
  }
  header {
    border: 1px solid #eeeeee;
    padding: 15px 25px;
    margin-top: -25px;
    display: flex;
    justify-content: space-between;
    position: relative;
  }
`;
const BranchContainer = styled.div`
  display: flex;
  flex-direction: column;
`;
const ChildrenContainer = styled.div`
  border-left: 2px dotted black;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  margin-left: 50px;
`;

const ChildBranch = styled(StyledBranch)`
  flex-direction: row;
  align-items: center;
  margin-top: 15px;
  margin-bottom: -8px;
  hr {
    margin-right: 5px;
  }
`;

const ButtonStyled = styled(Button)`
  height: 40px;
  width: 40px;
  font-size: 12px;
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  right: -15px;
  top: -15px;
`;

class Branch extends Component {
  onEdit = id => {
    this.props.dispatch(toggleModal('edit', id));
  };
  render() {
    const { title, min, max, count, id, children: branchChildren } = this.props;
    return (
      <StyledBranch>
        <hr />
        <BranchContainer>
          <header>
            {title}
            <ButtonStyled
              backgroundColor="#BB86FC"
              isCircle
              onClick={() => this.onEdit(id)}
            >
              Edit
            </ButtonStyled>
          </header>
          <ChildrenContainer>
            {branchChildren &&
              branchChildren.map((child, index) => {
                return (
                  <ChildBranch key={index}>
                    <hr />
                    {child}
                  </ChildBranch>
                );
              })}
          </ChildrenContainer>
        </BranchContainer>
      </StyledBranch>
    );
  }
}

export default connect()(Branch);
