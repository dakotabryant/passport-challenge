import React, { Component } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import Button from './Button';
import { toggleModal } from '../actions';

const StyledHeader = styled.header`
  background-color: #6200ee;
  color: white;
  padding: 25px;
  font-size: 25px;
  position: relative;
`;

const ButtonStyled = styled(Button)`
  position: absolute;
  left: 15px;
  top: 65px;
  transition: ease-in-out 0.2s;
  cursor: pointer;
  &:hover {
    background-color: #018786;
    transition: ease-in-out 0.2s;
  }
`;

class Header extends Component {
  clickHandler = () => {
    this.props.dispatch(toggleModal('create'));
  };
  render() {
    return (
      <StyledHeader>
        <div>Root</div>
        <ButtonStyled
          onClick={this.clickHandler}
          backgroundColor={'#03dac6'}
          isCircle
        >
          +
        </ButtonStyled>
      </StyledHeader>
    );
  }
}
export default connect()(Header);
