import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import NewBranchForm from './NewBranchForm';
import EditBranchForm from './EditBranchForm';
import {
  toggleModal,
  createBranch,
  updateBranch,
  deleteBranch,
} from '../actions';

const ModalContainer = styled.div`
  height: 100vh;
  width: 100%;
  background-color: rgba(0, 0, 0, 0.7);
  position: fixed;
  z-index: 10000;
`;

const ModalStyled = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  background-color: white;
  width: 750px;
  height: 500px;
  z-index: 10001;
`;

class Modal extends Component {
  closeHandler = () => {
    this.props.dispatch(toggleModal('create'));
  };
  createBranchHandler = formData => {
    this.props.dispatch(createBranch(formData));
  };
  editBranchHandler = updateObj => {
    this.props.dispatch(updateBranch(this.props.selectedBranchId, updateObj));
  };
  deleteBranchHandler = () => {
    this.props.dispatch(deleteBranch(this.props.selectedBranchId));
  };
  render() {
    const { modalType } = this.props;
    return (
      <ModalContainer>
        <ModalStyled>
          {modalType === 'create' && (
            <NewBranchForm
              onClose={this.closeHandler}
              onSubmit={this.createBranchHandler}
            />
          )}
          {modalType === 'edit' && (
            <EditBranchForm
              onClose={this.closeHandler}
              onEdit={this.editBranchHandler}
              onDelete={this.deleteBranchHandler}
            />
          )}
        </ModalStyled>
      </ModalContainer>
    );
  }
}

const mapStateToProps = state => {
  return {
    modalType: state.branchReducer.modalType,
    selectedBranchId: state.branchReducer.selectedBranchId,
  };
};

export default connect(mapStateToProps)(Modal);
