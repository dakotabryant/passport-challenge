import React, { Component } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import Button from './Button';
import Input from './Input';
import { flashError } from '../actions';

const FormStyled = styled.form`
  display: flex;
  flex-direction: column;
  padding: 75px 15px 0 15px;
  header {
    width: 100%;
    height: 50px;
    display: flex;
    align-items: center;
    padding-left: 15px;
    background-color: rgba(0, 0, 0, 0.03);
    position: absolute;
    top: 0;
    left: 0;
  }
`;
const ButtonStyled = styled(Button)`
  position: absolute;
  right: -25px;
  top: -25px;
  transition: ease-in-out 0.2s;
  cursor: pointer;
  &:hover {
    background-color: #018786;
    transition: ease-in-out 0.2s;
  }
`;
const SubmitButton = styled(Button)`
  margin-top: 25px;
  transition: ease-in-out 0.2s;
  &:hover {
    background-color: #018786;
    transition: ease-in-out 0.2s;
  }
`;

class NewBranchForm extends Component {
  constructor() {
    super();
    this.state = {
      title: '',
      min: '',
      max: '',
      count: '',
    };
  }
  onChangeHandler = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  handleSubmit = e => {
    const { title, min, max, count } = this.state;
    e.preventDefault();
    if (min > max) {
      return this.props.dispatch(
        flashError({
          errorType: 'error',
          message: "Please enter a minimum that's lower than your maximum ",
        })
      );
    }
    this.props.onSubmit({ title, min, max, count });
  };
  render() {
    const { onClose } = this.props;
    return (
      <FormStyled onSubmit={this.handleSubmit}>
        <ButtonStyled
          onClick={() => onClose()}
          backgroundColor={'#03dac6'}
          isCircle
        >
          X
        </ButtonStyled>
        <header>Create a new tree branch</header>
        <Input
          placeholder="Title"
          value={this.state.title}
          onChange={this.onChangeHandler}
          type="text"
          name="title"
          required
        />
        <Input
          placeholder="Minimum Random Number"
          value={this.state.min}
          onChange={this.onChangeHandler}
          type="number"
          name="min"
          min={0}
          max={this.state.max - 1}
          required
        />
        <Input
          placeholder="Maximum Random Number"
          value={this.state.max}
          onChange={this.onChangeHandler}
          type="number"
          name="max"
          min={this.state.min + 1}
          required
        />
        <Input
          placeholder="Number of integers to generate"
          value={this.state.count}
          onChange={this.onChangeHandler}
          type="number"
          name="count"
          required
          min="1"
          max="15"
        />
        <SubmitButton
          type="submit"
          backgroundColor="#03dac6"
          onClick={() => {}}
        >
          Submit
        </SubmitButton>
      </FormStyled>
    );
  }
}

export default connect()(NewBranchForm);
