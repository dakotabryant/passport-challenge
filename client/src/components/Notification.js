import React from 'react';
import styled from 'styled-components';

const StyledNotification = styled.div`
  position: fixed;
  bottom: 25px;
  right: 25px;
  padding: 25px;
  color: white;
  z-index: 10000000000;
  background-color: ${({ type }) => (type === 'error' ? '#a00020' : '#03DAC6')};
`;

const Notification = ({ type, message }) => {
  return <StyledNotification type={type}>{message}</StyledNotification>;
};

export default Notification;
