import React, { Component, Fragment } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import Button from './Button';
import Input from './Input';
import { flashError } from '../actions';

const FormStyled = styled.form`
  display: flex;
  flex-direction: column;
  padding: 75px 15px 0 15px;
  header {
    width: 100%;
    height: 50px;
    display: flex;
    align-items: center;
    padding-left: 15px;
    background-color: rgba(0, 0, 0, 0.03);
    position: absolute;
    top: 0;
    left: 0;
  }
`;
const ButtonStyled = styled(Button)`
  position: absolute;
  right: -25px;
  top: -25px;
  transition: ease-in-out 0.2s;
  cursor: pointer;
  &:hover {
    background-color: #018786;
    transition: ease-in-out 0.2s;
  }
`;
const SubmitButton = styled(Button)`
  margin-top: 25px;
  transition: ease-in-out 0.2s;
  font-size: 14px;
  &:hover {
    background-color: #018786;
    transition: ease-in-out 0.2s;
  }
`;
const DeleteButton = styled(SubmitButton)`
  position: absolute;
  right: -25px;
  bottom: -25px;
  width: 70px;
  &:hover {
    background-color: #a00020;
  }
`;
const ButtonGroup = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
`;

class EditBranchForm extends Component {
  constructor() {
    super();
    this.state = {
      title: '',
      min: '',
      max: '',
      count: '',
    };
  }
  onChangeHandler = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  handleSave = e => {
    const { title, min, max, count } = this.state;
    e.preventDefault();
    if (min > max) {
      return this.props.dispatch(
        flashError({
          errorType: 'error',
          message: "Please enter a minimum that's lower than your maximum ",
        })
      );
    }
    this.props.onEdit({ title, min, max, count });
  };
  handleDelete = e => {
    e.preventDefault();
    this.props.onDelete();
  };
  render() {
    const { onClose } = this.props;
    return (
      <Fragment>
        <FormStyled>
          <ButtonStyled
            onClick={() => onClose()}
            backgroundColor={'#03dac6'}
            isCircle
          >
            X
          </ButtonStyled>
          <header>Edit Current Branch</header>
          <Input
            placeholder="Title"
            value={this.state.title}
            onChange={this.onChangeHandler}
            type="text"
            name="title"
            required
          />
          <Input
            placeholder="Minimum Random Number"
            value={this.state.min}
            onChange={this.onChangeHandler}
            type="number"
            name="min"
            min={0}
            max={this.state.max - 1}
            required
          />
          <Input
            placeholder="Maximum Random Number"
            value={this.state.max}
            onChange={this.onChangeHandler}
            type="number"
            name="max"
            min={this.state.min + 1}
            required
          />
          <Input
            placeholder="Number of integers to generate"
            value={this.state.count}
            onChange={this.onChangeHandler}
            type="number"
            name="count"
            required
            min="1"
            max="15"
          />
          <ButtonGroup>
            <SubmitButton backgroundColor="#03dac6" onClick={this.handleSave}>
              Save Edits
            </SubmitButton>
          </ButtonGroup>
        </FormStyled>
        <DeleteButton backgroundColor="#b00020" onClick={this.handleDelete}>
          Delete
        </DeleteButton>
      </Fragment>
    );
  }
}

export default connect()(EditBranchForm);
