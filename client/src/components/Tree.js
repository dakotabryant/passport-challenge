import React from 'react';
import styled from 'styled-components';
import Branch from './Branch';

const TreeContainer = styled.div`
  display: flex;
  flex-direction: column;
  padding-top: 25px;
`;

const Tree = ({ branches }) => {
  return (
    <TreeContainer>
      {branches &&
        branches.map(({ title, min, max, children, _id }) => {
          return (
            <Branch
              title={title}
              min={min}
              max={max}
              children={children}
              id={_id}
              key={_id}
            />
          );
        })}
    </TreeContainer>
  );
};

export default Tree;
