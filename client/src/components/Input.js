import React from 'react';
import styled from 'styled-components';

const Label = styled.label`
  position: relative;
  font-size: 18px;
  margin: 20px 10px;
  &:before {
    transition: all 0.2s cubic-bezier(0.4, 0, 0.2, 1);
    content: '';
    position: absolute;
    top: calc(100% - 1px);
    left: 0;
    width: 100%;
    height: 1px;
    transform: scaleX(0);
    transform-origin: left;
    z-index: 1;
  }
  &:focus-within {
    &:before {
      transform: scaleX(1);
    }
  }
  input {
    background-color: transparent;
    border: none;
    border-bottom: 1px solid #eeeeee;
    color: black;
    font-size: 18px;
    padding: 5px 0px;
    transition: all 0.2s cubic-bezier(0.4, 0, 0.2, 1);
    transform: scale(1) translateY(0);
    width: 100%;
    position: relative;
    &:focus {
      outline: none;
    }
    &:focus ~ p {
      transform: scale(0.8) translateY(-30px);
      transform-origin: left;
      color: black;
    }
  }
  p {
    color: gray;
    font-size: 14px;
    position: absolute;
    top: 0;
    left: 0;
    transition: all 0.2s cubic-bezier(0.4, 0, 0.2, 1);
    opacity: 1;
    ${props => (props.hasValue ? 'opacity: 0;' : '')};
  }
`;

const Input = ({
  value,
  onFocus,
  onBlur,
  onChange,
  type,
  disabled,
  name,
  autoComplete,
  required,
  placeholder,
}) => (
  <Label hasValue={!!value && value.length > 0}>
    <input
      onFocus={onFocus}
      onBlur={onBlur}
      onChange={onChange}
      type={type}
      value={value}
      disabled={disabled}
      name={name}
      autoComplete={autoComplete}
      required={required}
    />
    <p>{placeholder}</p>
  </Label>
);

export default Input;
