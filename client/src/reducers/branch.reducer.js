import ActionTypes from '../actions/actionTypes';

const {
  CREATE_BRANCH,
  GET_BRANCHES,
  RENDER_BRANCHES,
  RENDER_UPDATE,
  UPDATE_BRANCH,
  RENDER_DELETED,
  FLASH_ERROR,
  TOGGLE_MODAL,
} = ActionTypes;

const branchInitialState = {
  branches: [],
  loading: false,
  showModal: false,
  modalType: 'create',
  selectedBranchId: '',
  error: null,
};

const branchReducer = (state = branchInitialState, action) => {
  switch (action.type) {
    case TOGGLE_MODAL:
      return {
        ...state,
        showModal: !state.showModal,
        modalType: action.modalType,
        selectedBranchId: action.branchId,
      };
    case FLASH_ERROR:
      return {
        ...state,
        error: { type: action.errorType, message: action.message },
      };
    case RENDER_BRANCHES:
      return {
        ...state,
        showModal: false,
        branches: [...state.branches, ...action.branches],
      };
    case CREATE_BRANCH:
      return {
        ...state,
        loading: true,
        showModal: false,
        error: { ...state.error, type: 'success', message: 'Created a branch' },
      };
    case GET_BRANCHES:
      return { ...state, loading: true };
    case RENDER_UPDATE:
      return {
        ...state,
        showModal: false,
        error: null,
        branches: state.branches.map(item => {
          if (item._id === action.id) {
            return { ...item, ...action.updatedBranch };
          }
          return item;
        }),
      };
    case RENDER_DELETED:
      return {
        ...state,
        showModal: false,
        error: null,
        branches: state.branches.filter(item => {
          if (item._id === action.id) return false;
          return true;
        }),
      };
    default:
      return state;
  }
};

export default branchReducer;
