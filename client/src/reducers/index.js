import { combineReducers } from 'redux';
import branchReducer from './branch.reducer';

export default combineReducers({ branchReducer });
