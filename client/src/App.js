import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled, { createGlobalStyle } from 'styled-components';
import {
  getBranches,
  createBranch,
  updateBranch,
  deleteBranch,
} from './actions';
import Header from './components/Header';
import Modal from './components/Modal';
import Tree from './components/Tree';
import Notification from './components/Notification';

const GlobalStyle = createGlobalStyle`
  * {
    font-family: 'Roboto', sans-serif;
    box-sizing: border-box;
  }
  body {
    margin: 0;
  }
  .App {
    padding-bottom: 150px;
  }
`;
export const ContainerStyled = styled.div`
  margin-left: 39px;
  border-left: 2px dashed black;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
`;

class App extends Component {
  componentDidMount() {
    this.props.dispatch(getBranches());
  }
  render() {
    const { branches, showModal, error } = this.props;
    return (
      <div className="App">
        <GlobalStyle />
        {showModal && <Modal />}
        {error && <Notification type={error.type} message={error.message} />}
        <Header />
        <ContainerStyled>
          <Tree branches={branches} />
        </ContainerStyled>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    branches: state.branchReducer.branches,
    showModal: state.branchReducer.showModal,
    error: state.branchReducer.error,
  };
};

export default connect(mapStateToProps)(App);
