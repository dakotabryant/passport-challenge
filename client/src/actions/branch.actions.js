import ActionTypes from './actionTypes';

const {
  CREATE_BRANCH,
  GET_BRANCHES,
  RENDER_BRANCHES,
  DELETE_BRANCH,
  UPDATE_BRANCH,
  RENDER_UPDATE,
  RENDER_DELETED,
  TOGGLE_MODAL,
  FLASH_ERROR,
} = ActionTypes;

export const createBranch = newBranch => ({
  type: CREATE_BRANCH,
  newBranch,
});

export const getBranches = () => ({
  type: GET_BRANCHES,
});

export const updateBranch = (id, updateObj) => ({
  type: UPDATE_BRANCH,
  id,
  updateObj,
});

export const renderUpdate = (id, updatedBranch) => ({
  type: RENDER_UPDATE,
  id,
  updatedBranch,
});

export const renderBranches = branches => ({
  type: RENDER_BRANCHES,
  branches,
});

export const deleteBranch = id => ({
  type: DELETE_BRANCH,
  id,
});

export const renderDeleted = id => ({
  type: RENDER_DELETED,
  id,
});

export const toggleModal = (modalType, branchId = '') => ({
  type: TOGGLE_MODAL,
  modalType,
  branchId,
});

export const flashError = ({ errorType, message }) => ({
  type: FLASH_ERROR,
  errorType,
  message,
});
