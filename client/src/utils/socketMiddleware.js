import io from 'socket.io-client';
import actionTypes from '../actions/actionTypes';
import { renderBranches, renderUpdate, renderDeleted } from '../actions';

const {
  CREATE_BRANCH,
  GET_BRANCHES,
  UPDATE_BRANCH,
  DELETE_BRANCH,
  TOGGLE_MODAL,
} = actionTypes;

let socket;

// this function positions itself after data comes back from the server. It is reacting to the server's emissions.

export function socketConnect(store) {
  if (process.env.NODE_ENV === 'production') {
    socket = io('https://queenfinder-server-owbzxdbnwq.now.sh/');
  } else {
    socket = io('http://localhost:8080');
  }
  socket.on('get-branches', branches => {
    store.dispatch(renderBranches(branches));
  });
  socket.on('create-branch', createdBranch => {
    store.dispatch(renderBranches([createdBranch]));
  });
  socket.on('update-branch', updatedBranch => {
    store.dispatch(renderUpdate(updatedBranch._id, updatedBranch));
  });
  socket.on('delete-branch', deletedBranch => {
    store.dispatch(renderDeleted(deletedBranch._id));
  });
}

// this is the middleware that essentially positions itself as requests go out

export function socketMiddleware(store) {
  return next => action => {
    const result = next(action);

    if (!socket) return;
    switch (action.type) {
      case CREATE_BRANCH:
        return socket.emit('create-branch', action.newBranch);
      case GET_BRANCHES:
        return socket.emit('get-branches');
      case UPDATE_BRANCH:
        return socket.emit('update-branch', {
          id: action.id,
          updateObj: action.updateObj,
        });
      case DELETE_BRANCH:
        return socket.emit('delete-branch', { id: action.id });
      default:
        break;
    }
  };
}
